<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home', [
        'title' => __('title.home'),
        'content' => __('content.home'),
    ]);
})->name('home');

Route::get('/the-beginning', function () {
    return view('content', [
        'title' => __('title.the-beginning'),
        'content' => __('content.the-beginning'),
        'image' => asset('images/Elijas_iela.jpg'),
    ]);
})->name('the-beginning');

Route::get('/academy-of-sciences', function () {
    return view('content', [
        'title' => __('title.academy-of-sciences'),
        'content' => __('content.academy-of-sciences'),
        'image' => asset('images/Akademija.jpg'),
    ]);
})->name('academy-of-sciences');

Route::get('/train-station', function () {
    return view('content', [
        'title' => __('title.train-station'),
        'content' => __('content.train-station'),
        'image' => asset('images/Stacija.jpg'),
    ]);
})->name('train-station');

Route::get('/city-canal', function () {
    return view('content', [
        'title' => __('title.city-canal'),
        'content' => __('content.city-canal'),
        'image' => asset('images/Kanals.jpg'),
    ]);
})->name('city-canal');

Route::get('/latvian-national-opera', function () {
    return view('content', [
        'title' => __('title.latvian-national-opera'),
        'content' => __('content.latvian-national-opera'),
        'image' => asset('images/Opera.jpg'),
    ]);
})->name('latvian-national-opera');

Route::get('/the-freedom-monument', function () {
    return view('content', [
        'title' => __('title.the-freedom-monument'),
        'content' => __('content.the-freedom-monument'),
        'image' => asset('images/Brivibas.jpg'),
    ]);
})->name('the-freedom-monument');

Route::get('/university-of-latvia', function () {
    return view('content', [
        'title' => __('title.university-of-latvia'),
        'content' => __('content.university-of-latvia'),
        'image' => asset('images/LU.jpg'),
    ]);
})->name('university-of-latvia');

Route::get('/{locale}', [\App\Http\Controllers\LocalizationController::class, 'index'])->name('locale');
