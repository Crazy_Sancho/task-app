<?php

return [

    'the-beginning' => 'The beginning of my way is Elijas street',
    'academy-of-sciences' => 'Latvian Academy of Sciences',
    'train-station' => ' Riga Train Station',
    'city-canal' => 'City canal',
    'latvian-national-opera' => 'Latvian National Opera and Ballet',
    'the-freedom-monument' => 'The Freedom Monument',
    'university-of-latvia' => 'The final stop of my way is University of Latvia',

];
