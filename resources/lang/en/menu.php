<?php

return [

    'home' => 'Home page',
    'the-beginning' => 'The beginning',
    'academy-of-sciences' => 'Academy of Sciences',
    'train-station' => 'Train Station',
    'city-canal' => 'City canal',
    'latvian-national-opera' => 'Latvian National Opera',
    'the-freedom-monument' => 'The Freedom Monument',
    'university-of-latvia' => 'University of Latvia',

    'copyright' => 'Zlata Potiļicina, zp18010'

];
