<?php

return [

    'the-beginning' => 'Ceļa sākums ir Elijas iela',
    'academy-of-sciences' => 'Latvijas Zinātņu akadēmija',
    'train-station' => 'Rīgas Pasažieru stacija',
    'city-canal' => 'Pilsētas kanāls',
    'latvian-national-opera' => 'Latvijas Nacionālā opera un balets',
    'the-freedom-monument' => 'Brīvības piemineklis',
    'university-of-latvia' => 'Ceļa beigās ir Latvijas Universitāte',

];
