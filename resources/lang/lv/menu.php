<?php

return [

    'home' => 'Sākumlapa',
    'the-beginning' => 'Ceļa sākums',
    'academy-of-sciences' => 'Zinātņu akadēmija',
    'train-station' => 'Pasažieru stacija',
    'city-canal' => 'Pilsētas kanāls',
    'latvian-national-opera' => 'Latvijas Nacionālā opera un balets',
    'the-freedom-monument' => 'Brīvības piemineklis',
    'university-of-latvia' => 'Latvijas Universitāte',

    'copyright' => 'Zlata Potiļicina, zp18010'

];
