@extends('_layout.base')

@section('content')
    <main class="main">
        <div class="bg-image">
            <img src="{{ $image }}" alt="@">
        </div>
        <div class="container">
            <h1>{{ $title }}</h1>
            <p>{{ $content }}</p>
        </div>
    </main>
@endsection
