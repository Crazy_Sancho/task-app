@extends('_layout.base')

@section('content')
    <main class="main home">
        <div class="container">
            <p>
                {{ $content }}
            </p>
            <img src="{{ asset('images/map.svg') }}" alt="">
        </div>
    </main>
@endsection
