<header class="header">
    <div class="container container_lg">
        <div class="nav">
            <a href="{{ route('home') }}" class="nav__item">{{ __('menu.home') }}</a>
        </div>
        <button type="button" class="nav__toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="nav nav__menu">
            <a href="{{ route('the-beginning') }}" class="nav__item">{{ __('menu.the-beginning') }}</a>
            <a href="{{ route('academy-of-sciences') }}" class="nav__item">{{ __('menu.academy-of-sciences') }}</a>
            <a href="{{ route('train-station') }}" class="nav__item">{{ __('menu.train-station') }}</a>
            <a href="{{ route('city-canal') }}" class="nav__item">{{ __('menu.city-canal') }}</a>
            <a href="{{ route('latvian-national-opera') }}" class="nav__item">{{ __('menu.latvian-national-opera') }}</a>
            <a href="{{ route('the-freedom-monument') }}" class="nav__item">{{ __('menu.the-freedom-monument') }}</a>
            <a href="{{ route('university-of-latvia') }}" class="nav__item">{{ __('menu.university-of-latvia') }}</a>

            @if(app()->getLocale() == 'lv')
                <a href="{{ route('locale', ['locale' => 'en']) }}" class="nav__item">EN</a>
            @else
                <a href="{{ route('locale', ['locale' => 'lv']) }}" class="nav__item">LV</a>
            @endif
        </div>
    </div>
</header>
