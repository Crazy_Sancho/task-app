const navToggle = document.querySelector(".nav__toggle");
const navMenu = document.querySelector(".nav__menu");

function openMobileNav() {
  navMenu.classList.add("opened");
}

function closeMobileNav() {
  navMenu.classList.remove("opened");
}

navToggle.addEventListener("click", () => {
  if (navMenu.classList.contains("opened")) {
    closeMobileNav();
  } else {
    openMobileNav();
  }
});

navMenu.addEventListener("click", (clickEvent) => {
  clickEvent.stopPropagation();
});

navMenu.addEventListener("click", closeMobileNav);
